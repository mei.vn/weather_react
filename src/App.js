import React, { Component } from 'react';
import { geolocated } from 'react-geolocated';
import Header from './components/Header';
import Content from './components/Content';
import Footer from './components/Footer';
class App extends Component {
	render() {
		return !this.props.isGeolocationAvailable ? <div>
				Your browser does not support Geolocation
			</div> : !this.props.isGeolocationEnabled ? <div>
				Geolocation is not enabled
			</div> : this.props.coords ? <div className="App">
				<Header current_lat={this.props.coords.latitude} current_lng={this.props.coords.longitude}/>
				<Content />
				<Footer />
			</div> : <div />;
	}
}
export default (geolocated())(App);
// export default App;