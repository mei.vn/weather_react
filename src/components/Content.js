import React, { Component } from 'react';
import PlacesAutocomplete from 'react-places-autocomplete';
import { geocodeByAddress, getLatLng } from 'react-places-autocomplete';
import { decodeData, callApiLat } from './Functions';
import  ContentResult  from './ContentResult';
class Content extends Component {
	constructor(props) {
		super(props);
		this.state = { address: '', showResult: false };
		this.onChange = address => this.setState({ address });
	}
	handleFormSubmit = event => {
		event.preventDefault();
		if (this.state.address !== '' && this.state.address !== undefined) {
			geocodeByAddress(this.state.address)
				.then(results => getLatLng(results[0]))
				.then(latLng => {
					console.log(latLng);
					// call api
					callApiLat(latLng.lat, latLng.lng, function(data) {
							data.cityname = this.state.address;
							var weather = decodeData(data);
							this.setState({ weather: weather, showResult: true });
						//  console.log(weather);
						}.bind(this));
				})
				.catch(error => console.error(error));
			}
	};
	render() {
		           const inputProps = { value: this.state.address, onChange: this.onChange, placeholder: 'Search Places', name: 'address' };
					const cssClasses = { root: 'form-group', input: 'form-control' };
		return <div id="main-section">
				<div className="container">
					<div className="section-title center">
						<h2>Check Weather</h2>
						<hr />
					</div>
					<h4>Where are you?</h4>
					<div className="row">
						<div className="col-md-10">
							<div className="form-group">
								<div id="locationField">
									<PlacesAutocomplete inputProps={inputProps} classNames={cssClasses} ref="aaa" />
									<button type="submit" className="btn btn-default btn-Checkweather" onClick={this.handleFormSubmit}>
										Check
									</button>
								</div>
								<p className="help-block text-danger" />
							</div>
						</div>
					</div>
			
						{this.state.showResult ? <ContentResult data={this.state.weather} /> : null}
					<div className="space" />
				</div>
			</div>;
	}
}
    export default Content;
