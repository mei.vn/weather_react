import React, { Component } from 'react';
import { callApiLat, decodeData } from './Functions';
import HeaderResultabc from './HeaderResult';
class Header extends Component {
	constructor(props) {
		super(props);
		this.state = { address: '', showResult: false, weather: false, cr_address: '' };
	}
	scrollBody = event => {
		event.preventDefault();
		var scrollToElement = require('scroll-to-element');
		var elmnt = document.getElementById('main-section');
		elmnt.scrollTop = elmnt.scrollHeight;
		scrollToElement(elmnt, { offset: 0, ease: 'inOutQuint', duration: 1000 });

	};
	componentDidMount() {
		// get location

		// getCurrentLocation(
		// 	this.props.current_lat,
		// 	this.props.current_lng,
		// 	function(res) {
		// 		this.setState({ cr_address: res });
		// 	}.bind(this)
		// );
		callApiLat(
			this.props.current_lat,
			this.props.current_lng,
			function(data) {
				if (!data) {
					this.setState({ errFoundCity: 'Can`t not get info from api!' });
				}
				if (data.message === undefined) {
					data.cityname = '';
					var weather = decodeData(data);
					// set icon
					var icon_tt = document.getElementById('icon-web');
					icon_tt.href = weather['icon'];
					this.setState({ weather: weather, showResult: true });
				} else {
					this.setState({ errFoundCity: data.message });
				}
				//  console.log(data);
			}.bind(this)
		);
	}
	render() {
		return <header className="text-center" name="home" id="header">
				<div className="intro-text container">
					{/* <h1>
						We are <span className="color">Grabbing Weather</span>
					</h1> */}
					<div className="weather container ">
						{this.state.errFoundCity ? this.state.errFoundCity : ''}
						{this.state.showResult ? <HeaderResultabc data={this.state.weather}/> : null}
					</div>
					<div className="space" />
					<div className="clearfix" />
					{/*  href="#main-section" */}
					<button className="btn btn-default btn-lg " onClick={this.scrollBody}>
						Check Weather in another places
					</button>
				</div>
				<div className="over" />
			</header>;
	}
}
export default Header;
