
const APP_ID = 'e1566353da91a4387b6f7f3508432834';
export const IMG_LINK = 'http://openweathermap.org/img/w/';
export const callApi = (city, callback) => {
	 fetch(`http://api.openweathermap.org/data/2.5/weather?&q=${city}&appid=${APP_ID}`)
			.then(res => res.json())
			.then(result => {
					callback(result);
				}, error => {
					callback(false);
				});
};
 export const getCurrentLocation = (latitude, longitude, callback) => {
	 fetch(`http://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&sensor=true`)
			.then(res => res.json())
			.then(result => {
				var address = [];
				if (result.error_message){
					address['error_message'] = result.error_message;
					 callback(address);
					 return;
				}
				 var res = result.results[0].address_components;
				    for (let item in res){
						if(res[item].types[0]  === 'country'){
							address['country'] = res[item].long_name;
						}
						if (res[item].types[0] === 'administrative_area_level_1') {
							address['city'] = res[item].long_name;
						}
						else if(res[item].types[0] === 'locality')
						{
							address['city'] = res[item].long_name;
						}
					}
				//  console.log(address);
				 callback(address);
				}, error => {
					console.log('fail load address');
					callback(false);
				});
 }
export const callApiLat = (lat,lng, callback) => {
	 fetch(`http://api.openweathermap.org/data/2.5/weather?&lat=${lat}&lon=${lng}&appid=${APP_ID}`)
			.then(res => res.json())
			.then(result => {
					callback(result);
				}, error => {
					console.log('failcall api');
					callback(false);
				});
};

export const getFullMinutes = Min => {
		if (Min < 10) {
			return '0' + Min;
		}
		return Min;
};

export const decodeData = (data) => {
										var res = data;
										if (data !== '-1') {
											res = [];
											res['cityname'] = data.cityname;
											res['name'] = data.name;
											res['country'] = data.sys.country;
											res['t_f'] = Math.round(data.main.temp * 9 / 5 - 459.67);
											res['t_c'] = Math.round(data.main.temp - 273.15);
											res['t_c_min'] = Math.round(data.main.temp_min - 273.15);
											res['t_c_max'] = Math.round(data.main.temp_max - 273.15);
											var t_sunset = new Date(data.sys.sunset * 1000);
											var t_sunrise = new Date(data.sys.sunrise * 1000);
											res['sunset_h'] = t_sunset.getHours();
											res['sunset'] = res['sunset_h'] + ':' + getFullMinutes(t_sunset.getMinutes());
											res['sunrise'] = t_sunrise.getHours() + ':' + getFullMinutes(t_sunrise.getMinutes());
											res['description'] = '';
											res['humidity'] = data.main.humidity + ' %';
											res['cloud'] = data.clouds.all + ' %';
											res['main'] = data.main;
											res['icon'] = IMG_LINK + data.weather[0].icon + '.png';
											res['rain'] = data.rain ? data.rain : false;
											res['snow'] = data.snow ? data.snow : false;
											res['wind_deg'] = degToDirection(data.wind.deg) ? ', ' + degToDirection(data.wind.deg) : '';
											res['wind'] = data.wind.speed + ' m/s' + res['wind_deg'];

											data.weather.forEach(function(val, i) {
												if (i === data.weather.length - 1) res['description'] = res['description'] + val.description;
												else res['description'] = res['description'] + val.description + ', ';
											});

											res['direction'] = "";
											//  res['description'] = data.weather[0].description;
											var time_stt = decodeTime();
											// end time
											// console.log(res['t_c']);
											if (res['t_c'] <= 16) {
												res['temp'] = 'cold';
												res['direction'] = 'Don’t forgot your coat and don’t get sick ';
											} else if (16 < res['t_c'] && res['t_c'] <= 21) {
												res['temp'] = 'cool';
												if (res['description'].includes('clear sky') || res['main'] === 'clear') res['direction'] = 'It’s a nice day out!';
											} else if (21 < res['t_c'] && res['t_c'] <= 26) {
												res['temp'] = 'warm';
											} else if (26 < res['t_c'] && res['t_c'] <= 33) {
												res['temp'] = 'quite hot';
												if (time_stt === 'morning' || time_stt === 'morning-2' || time_stt === 'noon' || time_stt === 'noon-2') res['direction'] = 'Don’t forgot your hat.';
											} else if (33 < res['t_c'] && res['t_c'] < 60) {
												res['temp'] = 'hot';
												if (time_stt === 'morning') res['direction'] = 'Don’t for get your sunscreen!';
											} else if (res['t_c'] > 60) {
												res['temp'] = 'very hot';
												if (time_stt === 'morning' || time_stt === 'morning-2' || time_stt === 'noon' || time_stt === 'noon-2') res['direction'] = 'Don’t for get your sunscreen!';
											} else res['temp'] = 'normal';
											res['determine'] = 'It’s a ' + res['temp'] + ' day';
											if (res['description'].includes('rain')) {
												res['direction'] = 'It will be rain, you should bring your umbrella';
											}
											// time
											res['description'] = capitalizeFirstLetter(res['description']);
										
										}
											return res;
									}

 export const decodeTime = () => {
   var hours =  new Date().getHours();
    // get current time:
       if(hours > 0 && hours < 10){
        return 'morning';

       }
         else if(hours >= 10 && hours < 12){
          return 'morning-2';

       }
       else if(hours >= 12 && hours < 14){
        return 'noon';
       }
        else if(hours >= 14 && hours < 17){
          return 'noon-2';
       }
          else if(hours >= 17 && hours < 21){
            return 'evening';
       }
          else if(hours >= 21 && hours < 0){
            return 'evening-2'
       }
}
export const capitalizeFirstLetter = (string) => {
	return string.charAt(0).toUpperCase() + string.slice(1);
}
    function degToDirection(num)
 {
	  var dir = "", val = 0;
  var arr = ["NNE" ,"NE","ENE","E","ESE", "SE", "SSE","S","SSW","SW","WSW","W","WNW","NW","NNW"];
  var res = { NNE: 'North-North-East', NE: 'North-East', ENE: 'East-North-East', E: 'East', ESE: 'East-South-East', SE: 'South-East', SSE: 'South-South-East', S: 'South', SSW: 'South-South-West', SW: 'South-West', WSW: 'West-South-West', W: 'West', WNW: 'West-North-West', NW: 'North-West', NNW: 'North-North-West' };
  if (num > 348.75 || num <= 11.25) {
   return "North";
  }
   else if (Number.isInteger(num/22.5) === true) {
   val = Math.floor((num/22.5)-.5);
    dir = arr[val - 1];
   return res[dir];
  } else {
   val= Math.floor((num/22.5)-.5);
  dir = arr[(val)];
  }
    return res[dir];
 }
export const checkDateStt = (description, determiners, sunset) => {
			var date = [];
			var header = document.getElementById('header');
			date['stt'] = 'clear';
			if (description.includes('Clear') || description.includes('clear')) {
				date['stt'] = 'clear';
			}
			if (description.includes('Cloud') || description.includes('cloud')) {
				date['stt'] = 'cloud';
			}
			if (description.includes('Rain') || description.includes('rain')) {
				date['stt'] = 'rain';
			}

			if (date['stt'] === 'clear') {
				header.style.backgroundImage = 'url(img/sun-morning.jpg)';
			} else if (date['stt'] === 'cloud') {
				header.style.backgroundImage = 'url(img/morning1.jpg)';
			} else if (date['stt'] === 'rain') {
				header.style.backgroundImage = 'url(img/rain.jpg)';
			}

			var hours = new Date().getHours();
			if (hours > 0 && hours < 10) {
				date['greet'] = 'Good Morning! Do you have a coffee?';
				if (date['stt'] === 'clear') 
				header.style.backgroundImage = 'url(img/morning.jpg)';
				else if (date['stt'] === 'rain') 
				header.style.backgroundImage = 'url(img/morning-cf.jpg)';
			} 
			else if (hours >= 10 && hours < 12) {
				date['greet'] = 'Good Morning!';
				header.style.backgroundImage = 'url(img/morning.jpg)';
				if (date['stt'] === 'clear') {
					date['determine'] = determiners + ', Don’t forgot your hat.';
					header.style.backgroundImage = 'url(img/sun-morning.jpg)';
				}
			}
			else if (hours >= 12 && hours < 13) {
				header.style.backgroundImage = 'url(img/lunch2.jpg)';
					date['greet'] = 'Good noon! Do you have a lunch?';
			}
		 else if (hours >= 13 && hours < 14) {
				date['greet'] = 'Good noon! Do you take a nap?';
				if (date['stt'] !== 'rain')
					header.style.backgroundImage = 'url(img/after-noon1.jpg)';
				// }
			} else if (hours >= 14 && hours <= sunset) {
						date['greet'] = 'Good afternoon!';
					if (date['stt'] !== 'rain')
				 header.style.backgroundImage = 'url(img/after-noon.jpg)';
					} 
			else if (hours > sunset && hours < 21) {
								date['greet'] = 'Good evening! Do you have an dinner?';
								if (date['stt'] !== 'rain') header.style.backgroundImage = 'url(img/evening.jpg)';
							} else if (hours >= 21) {
								if (date['stt'] !== 'rain') header.style.backgroundImage = 'url(img/evening.jpg)';
								date['greet'] = "It's time to bed! Good night.";
							}
			return date;
		}

	