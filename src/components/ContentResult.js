import React, { Component } from 'react';
// import PlacesAutocomplete from 'react-places-autocomplete';
// import { geocodeByPlaceId, getLatLng } from 'react-places-autocomplete';
// import { callApi} from './Functions';
class ContentResult extends Component {
	constructor(props) {
        super(props);
            this.state = { address: '' };
	}
    // get data and decode
	render() {
		const data = this.props.data;
		// console.log(data);
		return 		<div className="rs">
				<h4 className="results">{data.cityname}</h4>
				<table className="table">
					<tbody>
						<tr>
							<td>Degrees Fahrenheit</td>
							<td>{data.t_f} °F</td>
						</tr>
						<tr>
							<td>Degrees Celsius</td>
							<td>{data.t_c} °C</td>
						</tr>
						<tr>
							<td>Cloud</td>
							<td>{data.cloud}</td>
						</tr>
						<tr>
							<td>Wind</td>
							<td>{data.wind}</td>
						</tr>
						<tr>
							<td>Humidity</td>
							<td>{data.humidity}</td>
						</tr>
						<tr className="r_snow">
							<td>Snow</td>
							<td>{data.snow}</td>
						</tr>
						<tr>
							<td>Sunrise time</td>
							<td>{data.sunrise}</td>
						</tr>
						<tr>
							<td>Sunset time</td>
							<td>{data.sunset}</td>
						</tr>
						<tr>
							<td style={{ textAlign: 'center' }} className="description" colSpan={2}>
								{data.description}
							</td>
						</tr>
						<tr>
							<td className="determine" colSpan={2} style={{ textAlign: 'center' }}>
								{data.determine}
							</td>
						</tr>
						<tr className="r_direction" style={{ textAlign: 'center' }}>
							<td className="direction" colSpan={2}>
								{data.determine}
							</td>
						</tr>
					</tbody>
				</table>
			</div>;
			}
}
    export default ContentResult;
