import React, { Component } from 'react';
import { checkDateStt, getFullMinutes } from './Functions';
class HeaderResult extends Component {
    render() {
        const data = this.props.data;
        const stt = checkDateStt(data.description, data.determine, data.sunset_h);
		var objDate = new Date();
		var timeZone = 'Asia/Ho_Chi_Minh';
        const cr_date = objDate.toLocaleString('en', {
            day: 'numeric',timeZone: timeZone
        }) + ' - ' + objDate.toLocaleString('en', {
            month: 'long', timeZone: timeZone
        }) + ' - ' + objDate.toLocaleString('en', {
            year: 'numeric',
        }) + ', ' + objDate.toLocaleString('en', {
            hour: '2-digit',timeZone: timeZone,
            hour12: false,
        }) + ':' + getFullMinutes(objDate.toLocaleString('en', {
            minute: 'numeric',timeZone: timeZone
        }));


        return <div>
				<h2 id="cr_city" className="color">
					{data.name}, {data.country}
				</h2>
				<h3 className="cr-time" style={{ textAlign: 'left' }}>
										{cr_date}
				</h3> 
				<div className="date-stt">
					{stt.greet}
				</div> <p className="cr_determine" style={{ fontSize: 30 }}>
					{data.direction !== undefined && data.direction !== '' ? data.determine + ', ' + data.direction : data.determine}
				</p>
				<br />
				<div className="row">
					<div className="col-sm-6 temp-r" style={{ textAlign: 'left' }}>
						<img className="we-icon" style={{ width: 160 }} src={data.icon} alt="" />
						<span className="cr_temp_f">
							{data.t_f}° F 
						</span> | <span className="cr_temp_c"> {data.t_c}° C </span>
					</div> <div className="col-sm-6" style={{ textAlign: 'left', fontSize: 30 }}>
						<p style={{color: '#71bd46' }}>
							<b> {data.description} </b>
						</p>
						<p className="cr_cloud">
							Cloudiness: <b> {data.cloud} </b>
						</p> <p className="cr_wind">
							Wind speed: <b> {data.wind} </b>
						</p> <p className="cr_humidity">
							Humidity: <b> {data.humidity} </b>
						</p> <p className="cr_sunrise">
							Sunrise at: <b> {data.sunrise} </b>
						</p> <p className="cr_sunset">
							Sunset at: <b> {data.sunset} </b>
						</p>
					</div>
				</div>
			</div>;
    }
}
export default HeaderResult;